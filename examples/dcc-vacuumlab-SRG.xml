<dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="3.0.0" xsi:schemaLocation="https://ptb.de/dcc https://ptb.de/dcc/v3.0.0/dcc.xsd">
  <!--
    DCC - Digital Calibration Certificate
    Copyright (C) 2021 - Physikalisch-Technische Bundesanstalt

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    The development of minimum requirements for a digital calibration certificate (DCC) is partially funded and
    supported by the joint research project EMPIR 17IND02 (title: SmartCom).
    This project (17IND02) has received funding from the EMPIR programme co-financed by the Participating States
    and from the European Union's Horizon 2020 research and innovation programme.
    The further development of the digital calibration certificate (DCC) is partly supported by the lighthouse
    project GEMIMEG-II (GEMIMEG 01 MT20001E) funded by the German Federal Ministry for Economic Affairs and
    Energy (BMWi) and the project "International Standard Accident Number" (ISAN) funded by the Niedersächsiches
    Vorab initiative of the Volkswagen Foundation (VolkswagenStiftung).
    -->
  <dcc:administrativeData>
    <dcc:calibrationLaboratory>
      <dcc:contact>
        <dcc:name>
          <dcc:content lang="de">Physikalisch-Technische Bundesanstalt (PTB)</dcc:content>
        </dcc:name>
        <dcc:eMail>vacuum.cal@ptb.de</dcc:eMail>
        <dcc:location>
          <dcc:further>
            <dcc:content lang="en">Working Group 7.54 Vacuum Metrology</dcc:content>
            <dcc:content lang="de">Arbeitsgruppe 7.54 Vakuummetrologie</dcc:content>
          </dcc:further>
          <dcc:street>Abbestraße</dcc:street>
          <dcc:streetNo>2--12</dcc:streetNo>
          <dcc:postCode>10587</dcc:postCode>
          <dcc:city>Berlin</dcc:city>
          <dcc:countryCode>DE</dcc:countryCode>
        </dcc:location>
      </dcc:contact>
    </dcc:calibrationLaboratory>
    <dcc:dccSoftware>
      <dcc:software>
        <dcc:name>
          <dcc:content lang="en">vpy (calculation of analysis)</dcc:content>
        </dcc:name>
        <dcc:release>d8ee51d50dc8f0dd432db0be1197bb77a4105dff</dcc:release>
      </dcc:software>
      <dcc:software>
        <dcc:name>
          <dcc:content lang="en">vpy (calculation of result)</dcc:content>
        </dcc:name>
        <dcc:release>d8ee51d50dc8f0dd432db0be1197bb77a4105dff</dcc:release>
      </dcc:software>
      <dcc:software>
        <dcc:name>
          <dcc:content lang="en">cal (generation of json-dcc)</dcc:content>
        </dcc:name>
        <dcc:release>0.34.6</dcc:release>
      </dcc:software>
      <dcc:software>
        <dcc:name>
          <dcc:content lang="en">vl-dcc (generation of xml-dcc)</dcc:content>
        </dcc:name>
        <dcc:release>0.1.34</dcc:release>
      </dcc:software>
    </dcc:dccSoftware>
    <dcc:coreData>
      <dcc:countryCodeISO3166_1>DE</dcc:countryCodeISO3166_1>
      <dcc:usedLangCodeISO639_1>de</dcc:usedLangCodeISO639_1>
      <dcc:usedLangCodeISO639_1>en</dcc:usedLangCodeISO639_1>
      <dcc:mandatoryLangCodeISO639_1>en</dcc:mandatoryLangCodeISO639_1>
      <dcc:uniqueIdentifier>75998PTB20</dcc:uniqueIdentifier>
      <dcc:identifications>
        <dcc:identification>
          <dcc:issuer>calibrationLaboratory</dcc:issuer>
          <dcc:value>7.5-9.9-99-99-98</dcc:value>
          <dcc:name>
            <dcc:content lang="en">Reference No.</dcc:content>
            <dcc:content lang="de">Geschäftszeichen</dcc:content>
          </dcc:name>
        </dcc:identification>
      </dcc:identifications>
      <dcc:beginPerformanceDate>2020-06-17</dcc:beginPerformanceDate>
      <dcc:endPerformanceDate>2020-06-17</dcc:endPerformanceDate>
      <dcc:performanceLocation>laboratory</dcc:performanceLocation>
    </dcc:coreData>
    <dcc:items>
      <dcc:name>
        <dcc:content lang="en">Spinning Rotor Gauge (SRG)</dcc:content>
      </dcc:name>
      <dcc:item>
        <dcc:name>
          <dcc:content lang="en">A998</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">The device was shipped under atmospheric pressure. During the shipping the rotor was fixed by means of a magnet.

        The rotor was not baked before calibration.

        The previous calibration performed by PTB is described in the calibration certificate 75998\,PTB\,18.</dcc:content>
        </dcc:description>
        <dcc:manufacturer>
          <dcc:name>
            <dcc:content lang="en">The Manufacturer</dcc:content>
          </dcc:name>
        </dcc:manufacturer>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>manufacturer</dcc:issuer>
            <dcc:value>99999-998</dcc:value>
            <dcc:name>
              <dcc:content lang="en">Serial No.</dcc:content>
              <dcc:content lang="de">Kennnummer</dcc:content>
            </dcc:name>
          </dcc:identification>
        </dcc:identifications>
      </dcc:item>
    </dcc:items>
    <dcc:respPersons>
      <dcc:respPerson id="Responsible">
        <dcc:person>
          <dcc:name>
            <dcc:content>Givenname1 Name1</dcc:content>
          </dcc:name>
          <dcc:eMail>Givenname1.Name1@ptb.de</dcc:eMail>
        </dcc:person>
        <dcc:mainSigner>true</dcc:mainSigner>
      </dcc:respPerson>
      <dcc:respPerson id="Technician">
        <dcc:person>
          <dcc:name>
            <dcc:content>Givenname2 Name2</dcc:content>
          </dcc:name>
          <dcc:eMail>Givenname2.Name2@ptb.de</dcc:eMail>
        </dcc:person>
      </dcc:respPerson>
    </dcc:respPersons>
    <dcc:customer>
      <dcc:name>
        <dcc:content lang="en">Physikalisch-Technische Bundesanstalt</dcc:content>
      </dcc:name>
      <dcc:eMail>vacuum.cal@ptb.de</dcc:eMail>
      <dcc:location>
        <dcc:street>Abbestraße 2–12</dcc:street>
        <dcc:postCode>10597</dcc:postCode>
        <dcc:city>Berlin</dcc:city>
        <dcc:countryCode>DE</dcc:countryCode>
      </dcc:location>
    </dcc:customer>
  </dcc:administrativeData>
  <dcc:measurementResults>
    <dcc:measurementResult>
      <dcc:name>
        <dcc:content lang="de">Ergebnis der Kalibrierung</dcc:content>
        <dcc:content lang="en">Result of the calibration</dcc:content>
      </dcc:name>
      <dcc:usedMethods>
        <dcc:usedMethod>
          <dcc:name id="method_1">
            <dcc:content lang="en">Calibration procedure</dcc:content>
          </dcc:name>
          <dcc:description>
            <dcc:content lang="en">\label{sec:procedure} The effective accommodation coefficient \(\sigma(p)\) was determined between \SI{0.1}{\pascal} and \SI{1}{\pascal} by using the pressures generated in the PTB primary standard SE3 metrologically linked to the primary standard SE2, which is based on the static expansion method. The extrapolated effective accommodation coefficient \(\sigma_0\) (\(p &lt; \SI{1E-2}{\pascal}\)) was determined by extrapolation for \(p \rightarrow 0\) of the linear regression analysis of the data. A run-in-time of at least 12 h was provided before calibration.\par The measurements were performed with 6 readings for each of the 5 target points.\par The gas temperature \(T\) during calibration using the static expansion method with nitrogen was \SI{295.849+-0.033}{\kelvin} at a room temperature of \SI{296.16+-0.04}{\kelvin}.

        The device was operated with the following setup: \begin{quote} \sisetup{detect-all} \begin{tabular}{@{}&gt;{}r@{:~}l@{}} Diameter of the rotor (\(d\))&amp; \SI{4.5}{\milli\metre} \\ Density of the rotor (\(\rho\)) &amp; \SI{7.7}{\gram\per\centi\metre\tothe{3}} \\ Sigma &amp; \num{1.0} \\ Viscosity &amp; \num{0} \\ Unit &amp; 1/s (DCR) \end{tabular} \end{quote} For the measurement with nitrogen, a molar mass (\(M\)) of \( \SI{28.013}{\gram\per\mol} \) was used.

        A sample of the residual drag (RD) including its scatter was measured before the calibration at a base pressure below \(\SI{1E-6}{\pascal}\). Before the measurement with the calibration gas nitrogen, a RD of \SI{1.5429E-06}{\per\second} with a standard deviation of \SI{8.0E-10}{\per\second} was determined. Before each calibration point, RD (6 readings) was checked at the base pressure and subtracted from the subsequent measurement of the relative deceleration rate.</dcc:content>
          </dcc:description>
        </dcc:usedMethod>
        <dcc:usedMethod>
          <dcc:name id="method_2">
            <dcc:content lang="en">Accommodation coefficient</dcc:content>
          </dcc:name>
          <dcc:description>
            <dcc:content lang="en">The effective accommodation coefficient was obtained by: \[ \sigma(p_\text{cal}) = \frac{1}{p_\text{cal}} \frac{\pi\rho d}{20} \sqrt{\frac{8 R T}{\pi M}} \left( -\frac{\dot{\!\omega}}{\!\omega} - \text{RD}(\omega) \right) \] with \(R\) the gas constant, \(-\,\dot{\!\omega}/\omega\) the relative deceleration rate and \(p_\text{cal}\) the calibration pressure generated by the primary standard. For the calibration with the measurement gas nitrogen the extrapolated effective accommodation coefficient \(\sigma_0\) for \(p &lt; \SI{1E-2}{\pascal}\) was: \[ \sigma_0 = \lim_{p_\text{cal}\rightarrow 0} \sigma(p_\text{cal}) = \SI{0.9555}{}\quad (\text{nitrogen}) \] With this \(\sigma_0\) together with \(d\) and \(\rho\) from the parameter set under section \ref{sec:procedure}, the SRG controller will give the correct reading of pressure within the measurement uncertainties according to this calibration for pressures \(p &lt; \SI{1E-2}{\pascal}\). Alternatively, the pressure can be calculated from the relative deceleration rate according to \[ p_\text{ind} = \frac{\pi\rho d}{20 \sigma_0} \sqrt{\frac{8 R T}{\pi M}} \left( -\frac{\dot{\!\omega}}{\!\omega} - \text{RD}(\omega) \right)\,. \] In both cases offset and temperature have to be determined for each measurement.\par In the pressure range \(p &gt; \SI{1E-2}{\pascal}\) up to \(p = \SI{2}{\pascal}\), the real pressure \(p\) will be received by multiplication of the indicated pressure \(p_{\text{ind}}\) with a correction factor \(f(p_{\text{ind}})\): \[ p = p_{\text{ind}} f(p_{\text{ind}}) \] and viscosity = 0 entered in the controller. From our calibration, \(f(p_{\text{ind}})\) was obtained for this rotor by the following equation: \[ f(p_{\text{ind}}) = (1 + ( \SI{0.01796+-0.0006}{\per\pascal} ) \cdot p_{\text{ind}}) \quad (\text{nitrogen}) \]</dcc:content>
          </dcc:description>
        </dcc:usedMethod>
        <dcc:usedMethod>
          <dcc:name id="method_3">
            <dcc:content lang="en">Uncertainty</dcc:content>
          </dcc:name>
          <dcc:description>
            <dcc:content lang="en">The uncertainty of \(\sigma_0\) at the time of calibration is estimated to \SI{0.26}{\percent} (this includes the relative deviation of \(\sigma_0\) with different orientations after a new suspension). The uncertainty stated is the expanded measurement uncertainty obtained by multiplying the standard measurement uncertainty by the coverage factor \(k=2\). It has been determined in accordance with the “Guide to the Expression of Uncertainty in Measurement (GUM)”. The value of the measurand then normally lies, with a probability of approximately \SI{95}{\percent}, within the attributed coverage interval.</dcc:content>
          </dcc:description>
        </dcc:usedMethod>
      </dcc:usedMethods>
      <dcc:influenceConditions>
        <dcc:influenceCondition>
          <dcc:name>
            <dcc:content lang="de">Umgebungsbedingungen für das Messgas Stickstoff, Kalibrierung nach dem statischen Verfahren</dcc:content>
            <dcc:content lang="en">Ambient conditions for the test gas nitrogen, static expansion method</dcc:content>
          </dcc:name>
          <dcc:data>
            <dcc:quantity>
              <dcc:name>
                <dcc:content lang="de">Temperatur des Messgases</dcc:content>
                <dcc:content lang="en">gas temperature</dcc:content>
              </dcc:name>
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>295.849</si:value>
                <si:unit>\kelvin</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.033</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
            <dcc:quantity>
              <dcc:name>
                <dcc:content lang="de">Raumtemperatur</dcc:content>
                <dcc:content lang="en">room temperature</dcc:content>
              </dcc:name>
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>296.16</si:value>
                <si:unit>\kelvin</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.04</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:data>
        </dcc:influenceCondition>
      </dcc:influenceConditions>
      <dcc:results>
        <dcc:result>
          <dcc:name>
            <dcc:content lang="en">Result for the test gas nitrogen, static expansion method</dcc:content>
          </dcc:name>
          <dcc:data>
            <dcc:list>
              <dcc:name>
                <dcc:content lang="en">effective accommodation coefficient</dcc:content>
              </dcc:name>
              <dcc:quantity>
                <si:real xmlns:si="https://ptb.de/si">
                  <si:value>0.9555</si:value>
                  <si:unit>\one</si:unit>
                  <si:expandedUnc>
                    <si:uncertainty>0.0019</si:uncertainty>
                    <si:coverageFactor>2</si:coverageFactor>
                    <si:coverageProbability>0.95</si:coverageProbability>
                  </si:expandedUnc>
                </si:real>
              </dcc:quantity>
            </dcc:list>
            <dcc:list>
              <dcc:name>
                <dcc:content lang="en">viscosity correction factor</dcc:content>
              </dcc:name>
              <dcc:quantity>
                <si:real xmlns:si="https://ptb.de/si">
                  <si:value>0.01796</si:value>
                  <si:unit>\kilogram\tothe{-1}\metre\second\tothe{2}</si:unit>
                  <si:expandedUnc>
                    <si:uncertainty>0.0006</si:uncertainty>
                    <si:coverageFactor>2</si:coverageFactor>
                    <si:coverageProbability>0.95</si:coverageProbability>
                  </si:expandedUnc>
                </si:real>
              </dcc:quantity>
            </dcc:list>
          </dcc:data>
        </dcc:result>
      </dcc:results>
    </dcc:measurementResult>
  </dcc:measurementResults>
</dcc:digitalCalibrationCertificate>
